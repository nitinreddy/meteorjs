# MeteorJS

This repo is basically dealing with MeteorJS framework as purely Isomorphic JS Framework.


Prerequisites for the application

Meteor JS
Download the meteor distribution (https://www.meteor.com/install)

For linux/mac
curl https://install.meteor.com/ | sh
Setup

Clone from the current repository
Running the app

Move to the project directory
  $ cd /isomorphicjs
Start the server (by default runs the server on port 3000)
  $ metoer
or

  $ meteor --port 4000
Open the app in your browser (localhost:3000)
